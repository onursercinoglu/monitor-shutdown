# README #

monitor-shutdown includes two simple scripts for monitoring power and shutting down devices in case of a power outage.

### Why use it ? ###

monitor-shutdown is basically useful in cases of power outage. A power outage without prior notification can cause serious 
damage to electronics, especially the hard drives installed on computers. monitor-shutdown includes two simple scripts that are meant to be 
used on a monitoring device and one (or more) computers that needs to be terminated safely in case of a long-lasting power outage occuring
while you're away.

### How to use it? ###

First, make sure that you have Python 3.x installed on all the computers you will use. 

Apart from the computers you want to protect (obviously), you basically need three hardware components:

** a spare computer which you rarely use and doesn't mean a lot to you
HINT: you can use a Raspberry Pi for this! :) **

** a router **

** a UPS **

You **MUST** have all of the above or else monitor-shutdown will be of no use.

We will call the "spare computer" "the listener" and other computer(s) "the talker(s)". 

Connect the listener to the regular power plug. Connect the talkers to the UPS power. Connect all of the devices to the router,  
creating a Local Area Network (LAN).

On the listener, start a terminal and run:

`./listen.py`

On the talkers, edit the talk.py and replace `BIOE-BANSHEE.local` with whatever the name of the listener is on the LAN.
You can also replace it with the local IP adress of the listener. Then run:

`sudo ./talk.py`

**It is crucial to call with root access, or else shutdown will not initiate!**

You will see that, on the listener terminal, several lines starting with "Connection accepted from..." are being printed to the screen. 
On talker terminals, you will see lines starting with "I received your message..." are being printed to the screen.

So what's happening? As soon as you call listen.py on the listener, the listener starts to listen a specific port (9077) for incoming
connection requests from the network. When you call ./talk.py on the talkers, they start trying to connect to the listener using the same port. 
If the connection is successful, a message is printed on the screen. This loop goes on forever as long as the listener can be accessed 
via the LAN. 

Now, suppose a power outage occurs. The first computer that goes down will be the listener, since it won't have any UPS power to back up.
The talkers will be online for as long as the UPS power can hold them. Since at that moment the listener will not be able to accept
incoming requests, the talkers will receive no messages. In this case the talkers will try to re-connect three more times after short pauses,
taking into account a momentary failure in LAN connections. If, even after three times no connection can be made, the talkers will assume
a power outage occured and shut themselves down. All this happens ideally before the UPS battery runs out of power. 

The scripts are very simple and easily configurable for your own needs.

### CREDITS ###

I would like to express my gratitude to AYEDAŞ, our power supplier on the Anatolian side of Istanbul,
for their persistence and determination for cutting off power in the middle of the night at least several times a week for 
God-knows-why reason. I couldn't have done this work if they supplied steady and consistent power to our neighborhood and I 
hadn't lost precious data on the workstations I use at my workplace, which need to run HPC simulations 24/7. 

Yaratıcılığımı teşvik ettiğiniz için sizlere minnetarım!
