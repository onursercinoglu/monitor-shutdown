#!/usr/bin/env python

import socket

s = socket.socket()
host = socket.gethostname()
port = 9077 
s.bind(('',port))
s.listen(5)

while True:
	c, addr = s.accept()
	print("Connection accepted from " + repr(c))
	c.send(repr(host) + ": I received your message. I'm still alive and well, no power cuts! :)")
	#c.close()