#!/usr/bin/env python

import socket
import time
import os
import logging

# TRANSFER ALL PRINT STATEMENTS TO A LOG FILE

loggingFormat = '%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s'
logging.basicConfig(format=loggingFormat,datefmt='%d-%m-%Y:%H:%M:%S',level=logging.DEBUG,
	filename='monitorLog.log')
logger = logging.getLogger(__name__)
	
# Also print messages to the terminal
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(logging.Formatter(loggingFormat))
logger.addHandler(console)

logger.info('Started checking BANSHEE.')

port = 9077
continueFlag = True
passCount = 0
while continueFlag:
	try:
		s = socket.socket()
		s.connect(("BIOE-BANSHEE.local",port))
		logger.info(s.recv(1024))
		passCount = 0
	except:
		if passCount > 3:
			logger.info("Could not connect to the report for %i times already." % passCount)
			logger.info("Probably a power cut happened, so I'm shutting myself down" )
			continueFlag = False
		else:
			logger.info("Could not connect to reporter. Will try again soon.")
			passCount += 1
			time.sleep(15)
	s.close()
	time.sleep(5)

logger.info("Shutting down now. Goodbye.")
os.system("shutdown now -h")
